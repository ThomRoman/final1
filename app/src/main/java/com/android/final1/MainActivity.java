package com.android.final1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText email, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = (EditText) findViewById(R.id.txtemail);
        password = (EditText) findViewById(R.id.txtpassword);
    }
    public void Enviar(View view) {


        if (email.getText().toString().isEmpty()) {
            Toast.makeText(this, "Campo Email vacio", Toast.LENGTH_LONG).show();
        } else {
            if (password.getText().toString().isEmpty()) {
                Toast.makeText(this, "Campo contraseña Vacio", Toast.LENGTH_LONG).show();
            } else {
                if (password.getText().toString().length() < 4) {
                    Toast.makeText(this, "Tiene que contener al menos 4 digitos", Toast.LENGTH_LONG).show();
                } else {
                    Intent recoger = new Intent(this, RecogerDatos.class);

                    //enviar datos email
                    recoger.putExtra("dato1", email.getText().toString());
                    //enviar datos Contrasenia
                    recoger.putExtra("dato2", password.getText().toString());

                    startActivity(recoger);
                }
            }
        }
    }
}